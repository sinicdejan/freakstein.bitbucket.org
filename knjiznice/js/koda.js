
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

generirajPodatke(0, function() {
        generirajPodatke(1, function() {
            generirajPodatke(2, function() {
      //All three functions have completed, in order.
            });
        });
    });
/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta, callback) {

  var sessionId = getSessionId();
  // TODO: Potrebno implementirati
  if(stPacienta == 0) {
    var ime = "Janez";
    var priimek = "Novak";
    var datumRojstva = "1955-5-21T19:30";
    var spol = "MALE";
  }
  if(stPacienta == 1) {
    var ime = "Metka";
    var priimek = "Pridite";
    var datumRojstva = "1975-9-5T19:30";
    var spol = "FEMALE";
  }
  if(stPacienta == 2) { 
    var ime = "Adrijan";
    var priimek = "Jurašek";
    var datumRojstva = "1995-12-1T19:30";
    var spol = "MALE";
  }
    $.ajaxSetup({
        headers: {
            "Ehr-Session": sessionId
        }
    });
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        success: function (data) {
            var ehrId = data.ehrId;
            // build party data
            var partyData = {
                firstNames: ime,
                lastNames: priimek,
                dateOfBirth: datumRojstva,
                gender: spol,
                partyAdditionalInfo: [
                    {
                        key: "ehrId",
                        value: ehrId
                    }
                ]
            };
            $.ajax({
            url: baseUrl + "/demographics/party",
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(partyData),
            success: function (party) {
                if (party.action == 'CREATE') {
                    pridobiPodatkiPoEhrIdInShraniVSelectList(stPacienta, ehrId, callback);
                }
            }
        });
        }
    });
  //return ehrId;
}

function generirajVsePodatke() {
    generirajPodatke(0, function() {
        generirajPodatke(1, function() {
            generirajPodatke(2, function() {
                  alert("Osebe so bile osvežene in dodane na seznam.");
            });
        });
    });
}

function pridobiPodatkiPoEhrIdInShraniVSelectList(st, ehrId, callback) {
    var sessionId = getSessionId();
    $.ajaxSetup({
        headers: {
            "Ehr-Session": sessionId
        }
    });
    var searchData = [
        {key: "ehrId", value: ehrId}
    ];
    $.ajax({
        url: baseUrl + "/demographics/party/query",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(searchData),
        success: function (res) {
            for (i in res.parties) {
                var party = res.parties[i];
                var changeItem = document.getElementById("selectList");
                //addItem.options.add(new Option(party.firstNames + " " + party.lastNames, ehrId));
                changeItem.options[st].value = ehrId;
                
            }
        }
    });
    callback();
}

function izracunajStarost(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

function prikaziPodatkeOPacientu(ehrId) {
    //var ehrId = document.getElementById("kreirajEhrID").value;
    var sessionId = getSessionId();
    $.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    type: 'GET',
    headers: {
        "Ehr-Session": sessionId
    },
    success: function (data) {
        var party = data.party;
        
        // Name
        $("#patient-name").html(party.firstNames + ' ' + party.lastNames);

        // Complete age
        var age = izracunajStarost(party.dateOfBirth);
        //var age = getAge(formatDateUS(party.dateOfBirth));
        $(".patient-age").html(age);

        // Date of birth
        var date = new Date(party.dateOfBirth);
        var mesec = date.getMonth() + 1;
        var stringDate =  date.getDate() +'. '+ mesec + '.' + date.getFullYear();
        $(".patient-dob").html(stringDate);

        // Gender
        var gender = party.gender;
        $("#patient-gender").html(gender.substring(0, 1) + gender.substring(1).toLowerCase());
        
        gender = gender.toLowerCase(); //check gender
        var src = "knjiznice/img/body-blank-" + gender + ".png";
        
        document.getElementById("patient-body").src = src;
        
        src = "knjiznice/img/" + gender + ".png";

        document.getElementById("patient-picture").src = src;
        // Patient's picture
        izrisiVisino(ehrId);
        graf();
        pridobiVitalneZnake(ehrId);
    }
});
}

function vrniSpol(ehrId) {
    var sessionId = getSessionId();
    $.ajaxSetup({
        headers: {
            "Ehr-Session": sessionId
        }
    });
    var searchData = [
        {key: "ehrId", value: ehrId}
    ];
    $.ajax({
        url: baseUrl + "/demographics/party/query",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(searchData),
        success: function (res) {
            if(!res) alert("da");
            for (i in res.parties) {
                var party = res.parties[i];
                return(party.gender);
            }
        }
    });
}

function izbiraPacienta() {
    $("#kreirajEhrID").val(document.getElementById("selectList").value);
}

function nastaviPodatkeOPacientu() {
    var sessionId = getSessionId();
    var ehrId = document.getElementById("kreirajEhrID").value;
    var compositionData;
    var e = document.getElementById("selectList");
    if(e.options[0].value == ehrId) {
        compositionData = {
            "ctx/time": "2014-3-19T13:10Z",
            "ctx/language": "en",
            "ctx/territory": "CA",
            "vital_signs/body_temperature/any_event/temperature|magnitude": 37.8,
            "vital_signs/body_temperature/any_event/temperature|unit": "°C",
            "vital_signs/blood_pressure/any_event/systolic": 130,
            "vital_signs/blood_pressure/any_event/diastolic": 100,
            "vital_signs/height_length/any_event/body_height_length": 185,
            "vital_signs/body_weight/any_event/body_weight": 105.5,
            "vital_signs/indirect_oximetry:0/spo2|numerator": 92
        };
    }
    else if(e.options[1].value == ehrId) {
        compositionData = {
            "ctx/time": "2014-3-19T13:10Z",
            "ctx/language": "en",
            "ctx/territory": "CA",
            "vital_signs/body_temperature/any_event/temperature|magnitude": 37.1,
            "vital_signs/body_temperature/any_event/temperature|unit": "°C",
            "vital_signs/blood_pressure/any_event/systolic": 110,
            "vital_signs/blood_pressure/any_event/diastolic": 80,
            "vital_signs/height_length/any_event/body_height_length": 160,
            "vital_signs/body_weight/any_event/body_weight": 60.2,
            "vital_signs/indirect_oximetry:0/spo2|numerator": 95
        };
    }
    else if(e.options[2].value == ehrId) {
        compositionData = {
            "ctx/time": "2014-3-19T13:10Z",
            "ctx/language": "en",
            "ctx/territory": "CA",
            "vital_signs/body_temperature/any_event/temperature|magnitude": 38.5,
            "vital_signs/body_temperature/any_event/temperature|unit": "°C",
            "vital_signs/blood_pressure/any_event/systolic": 120,
            "vital_signs/blood_pressure/any_event/diastolic": 90,
            "vital_signs/height_length/any_event/body_height_length": 178,
            "vital_signs/body_weight/any_event/body_weight": 75.0,
            "vital_signs/indirect_oximetry:0/spo2|numerator": 90
        };
    } else {
        prikaziPodatkeOPacientu(ehrId);
    }
    
    $.ajaxSetup({
    headers: {
        "Ehr-Session": sessionId
    }
    });
    var queryParams = {
        "ehrId": ehrId,
        templateId: 'Vital Signs',
        format: 'FLAT',
        committer: 'Belinda Nurse'
    };
    $.ajax({
        url: baseUrl + "/composition?" + $.param(queryParams),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(compositionData),
        success: function (res) {
            /*
            $("#header").html("Store composition");
            $("#result").html(res.meta.href);
            */
            //if(res) izrisiVisino(ehrId);
            if(res) prikaziPodatkeOPacientu(ehrId);
        }
    });
}

function pridobiVitalneZnake(ehrId) {
    var sessionId = getSessionId();
    $.ajax({
    url: baseUrl + "/view/" + ehrId + "/weight",
    type: 'GET',
    headers: {
        "Ehr-Session": sessionId
    },
    success: function (res) {
        document.getElementById("nasvet").innerHTML = document.getElementById("nasvet").innerHTML + "Teža: " + res[res.length-1].weight + " kg" + "<br />";
    }
});
    var sessionId = getSessionId();
    $.ajax({
    url: baseUrl + "/view/" + ehrId + "/body_temperature",
    type: 'GET',
    headers: {
        "Ehr-Session": sessionId
    },
    success: function (res) {
        document.getElementById("nasvet").innerHTML = document.getElementById("nasvet").innerHTML + "Temperatura: " + res[res.length-1].temperature + " °C" + "<br />";
    }
});

    var sessionId = getSessionId();
    $.ajax({
    url: baseUrl + "/view/" + ehrId + "/blood_pressure",
    type: 'GET',
    headers: {
        "Ehr-Session": sessionId
    },
    success: function (res) {
        document.getElementById("nasvet").innerHTML = document.getElementById("nasvet").innerHTML + "Sistolični: " + res[res.length-1].systolic + " mm[Hg]" + "<br />";
    }
});

    var sessionId = getSessionId();
    $.ajax({
    url: baseUrl + "/view/" + ehrId + "/blood_pressure",
    type: 'GET',
    headers: {
        "Ehr-Session": sessionId
    },
    success: function (res) {
        document.getElementById("nasvet").innerHTML = document.getElementById("nasvet").innerHTML + "Diastolični: " + res[res.length-1].diastolic + " mm[Hg]" + "<br />";
    }
});

    var sessionId = getSessionId();
    $.ajax({
    url: baseUrl + "/view/" + ehrId + "/spO2",
    type: 'GET',
    headers: {
        "Ehr-Session": sessionId
    },
    success: function (res) {
        document.getElementById("nasvet").innerHTML = document.getElementById("nasvet").innerHTML + "Nasičenost krvi s kisikom: " + res[res.length-1].spO2 + " %" + "<br />";
    }
});
}

function izrisiVisino(ehrId) {
    var sessionId = getSessionId();
    $.ajax({
    url: baseUrl + "/view/" + ehrId + "/height",
    type: 'GET',
    headers: {
        "Ehr-Session": sessionId
    },
    success: function (res) {
        // display newest record
        $('.height-placeholder-value').text(res[0].height);
        $('.height-placeholder-unit').text(res[0].unit);
    }
 });
}
    function graf() {
    /* Radar chart design created by Nadieh Bremer - VisualCinnamon.com */
  
		////////////////////////////////////////////////////////////// 
		//////////////////////// Set-Up ////////////////////////////// 
		////////////////////////////////////////////////////////////// 
		var margin = {top: 40, right: 100, bottom: 100, left: 100},
			width = Math.min(360, window.innerWidth - 200) - margin.left - margin.right,
			height = Math.min(width, window.innerHeight - margin.top - margin.bottom - 20);
				
		////////////////////////////////////////////////////////////// 
		////////////////////////// Data ////////////////////////////// 
		////////////////////////////////////////////////////////////// 
		var data = [
				  [//zgornjaMeja
					{axis:"Temperatura",value:0.40},
					{axis:"Sistolični krvni tlak",value:0.40},
					{axis:"Diastolični krvni tlak",value:0.40},
					{axis:"Nasičenost krvi s kisikom",value:0.40}						
				  ],[//spodnjaMeja
					{axis:"Temperatura",value:0.20},
					{axis:"Sistolični krvni tlak",value:0.20},
					{axis:"Diastolični krvni tlak",value:0.20},
					{axis:"Nasičenost krvi s kisikom",value:0.20}						
				  ],[//pacient
					{axis:"Temperatura",value:0.30},
					{axis:"Sistolični krvni tlak",value:0.30},
					{axis:"Diastolični krvni tlak",value:0.30},
					{axis:"Nasičenost krvi s kisikom",value:0.30}						
				  ]
				];
		////////////////////////////////////////////////////////////// 
		//////////////////// Draw the Chart ////////////////////////// 
		////////////////////////////////////////////////////////////// 
		var color = d3.scale.ordinal()
			.range(["#EDC951","#CC333F","#00A0B0"]);
			
		var radarChartOptions = {
		  w: width,
		  h: height,
		  margin: margin,
		  maxValue: 0.5,
		  levels: 5,
		  roundStrokes: true,
		  color: color
		};
		//Call function to draw the Radar chart
		RadarChart(".radarChart", data, radarChartOptions);
    }

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
